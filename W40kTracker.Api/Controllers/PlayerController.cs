﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using W40kTracker.Data;
using W40kTracker.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace W40kTracker.App.Controllers
{
	[Route("api/players")]
	[ApiController]
	public class PlayerController : ControllerBase
	{
		private readonly DataContext _dataContext;

		public PlayerController(DataContext dataContext)
		{
			_dataContext = dataContext;
		}

		[HttpGet("game/{id}")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		public async Task<ActionResult<IEnumerable<Player>>> List(int id)
		{
			Game game = await _dataContext.Games
				.Include(game => game.Players)
				.FirstOrDefaultAsync(game => game.GameId == id);

			return game.Players ?? new List<Player>();
		}

		[HttpGet("{id}")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		public async Task<ActionResult<Player>> Get(int id)
		{
			Player player = await _dataContext.Players.FindAsync(id);

			if (player == null)
				return NotFound();

			return player;
		}

		[HttpPost("game/{id}")]
		[ProducesResponseType(StatusCodes.Status201Created)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		public async Task<ActionResult<Game>> Create(int id, [FromBody] Player player)
		{
			Game game = await _dataContext.Games
				.FindAsync(id);

			game.Players.Add(player);

			await _dataContext.SaveChangesAsync();

			return CreatedAtAction(nameof(Get), new { id = player.PlayerId }, player);
		}

		[HttpPut]
		[ProducesResponseType(StatusCodes.Status204NoContent)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		public async Task<ActionResult> Edit([FromBody] Player player)
		{
			try
			{
				_dataContext.Update(player);
				await _dataContext.SaveChangesAsync();
			}
			catch (Exception)
			{
				return BadRequest("Error while editing player");
			}
			return NoContent();
		}
	}
}
