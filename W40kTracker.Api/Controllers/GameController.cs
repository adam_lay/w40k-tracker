﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using W40kTracker.Data;
using W40kTracker.Data.Models;
using Microsoft.EntityFrameworkCore;
using W40kTracker.Api.Hubs;

namespace W40kTracker.App.Controllers
{
	[Route("api/games")]
	[ApiController]
	public class GameController : ControllerBase
	{
		private readonly DataContext _dataContext;
		private readonly IHubContext<GameHub> _gameHubContext;

		public GameController(DataContext dataContext, IHubContext<GameHub> gameHubContext)
		{
			_dataContext = dataContext;
			_gameHubContext = gameHubContext;
		}

		[HttpGet]
		[ProducesResponseType(StatusCodes.Status200OK)]
		public async Task<ActionResult<IEnumerable<Game>>> List()
		{
			return await _dataContext.Games
				.Where(game => game.Active)
				.ToListAsync();
		}

		[HttpGet("{id}")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		public async Task<ActionResult<Game>> Get(int id)
		{
			Game game = await _dataContext.Games
				.Include(g => g.Players)
				.Include(g => g.CurrentPhase)
				.Include(g => g.CurrentPlayer)
				.Include(g => g.VictoryPointsRecords)
				.FirstOrDefaultAsync(g => g.GameId == id);

			if (game == null)
				return NotFound();

			game.Players = game.Players.OrderBy(p => p.Initiative).ToList();

			return game;
		}

		[HttpPost]
		[ProducesResponseType(StatusCodes.Status201Created)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		public async Task<ActionResult<Game>> Create([FromBody] Game game)
		{
			await _dataContext.Games.AddAsync(game);
			await _dataContext.SaveChangesAsync();

			await _gameHubContext.Clients.All.SendAsync("NewGame", game);

			return CreatedAtAction(nameof(Get), new { id = game.GameId }, game);
		}

		[HttpPut]
		[ProducesResponseType(StatusCodes.Status204NoContent)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		public async Task<ActionResult> Edit([FromBody] Game game)
		{
			try
			{
				_dataContext.Update(game);
				await _dataContext.SaveChangesAsync();
			}
			catch (Exception)
			{
				return BadRequest("Error while editing item");
			}
			return NoContent();
		}
	}
}
