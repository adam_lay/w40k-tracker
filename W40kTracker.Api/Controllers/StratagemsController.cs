﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using W40kTracker.Data;
using W40kTracker.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace W40kTracker.App.Controllers
{
	[Route("api/stratagems")]
	[ApiController]
	public class StratagemsController : ControllerBase
	{
		private readonly DataContext _dataContext;

		public StratagemsController(DataContext dataContext)
		{
			_dataContext = dataContext;
		}

		[HttpGet("{factionId}")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		public async Task<ActionResult<IEnumerable<Stratagem>>> Get(int factionId)
		{
			var stratagems = await _dataContext.Stratagems
				.Where(s => s.FactionId == factionId)
				.ToListAsync();

			return stratagems;
		}
	}
}
