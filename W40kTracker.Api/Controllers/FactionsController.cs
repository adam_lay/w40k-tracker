﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using W40kTracker.Data;
using W40kTracker.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace W40kTracker.App.Controllers
{
	[Route("api/factions")]
	[ApiController]
	public class FactionsController : ControllerBase
	{
		private readonly DataContext _dataContext;

		public FactionsController(DataContext dataContext)
		{
			_dataContext = dataContext;
		}

		[ProducesResponseType(StatusCodes.Status200OK)]
		public async Task<ActionResult<List<Faction>>> Get()
		{
			var factions = await _dataContext.Factions.ToListAsync();

			return factions;
		}
	}
}
