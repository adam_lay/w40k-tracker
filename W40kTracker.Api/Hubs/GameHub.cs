﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using W40kTracker.Data;
using W40kTracker.Data.Models;

namespace W40kTracker.Api.Hubs
{
	public class GameHub : Hub
	{
		private readonly DataContext _dataContext;

		public GameHub(DataContext dataContext)
		{
			_dataContext = dataContext;
		}

		public async Task PlayerJoined(int playerId)
		{
			Player player = await _dataContext.Players.FindAsync(playerId);

			// Echo to all
			await Clients.All.SendAsync(nameof(PlayerJoined), player);
		}

		public async Task SetInitiative(int playerId, int initiative)
		{
			Player player = await _dataContext.Players.FindAsync(playerId);
			player.Initiative = initiative;
			await _dataContext.SaveChangesAsync();

			// Echo to all
			await Clients.All.SendAsync(nameof(SetInitiative), playerId, initiative);
		}

		public async Task StartGame(int gameId)
		{
			Game game = await _dataContext.Games
				.Include(g => g.Players)
				.FirstOrDefaultAsync(g => g.GameId == gameId);
			game.Turn = 1;
			game.CurrentPhase = await _dataContext.Phases.FirstAsync();
			game.CurrentPlayer = game.Players.OrderBy(p => p.Initiative).First();
			await _dataContext.SaveChangesAsync();

			// Echo to all
			await Clients.All.SendAsync(nameof(StartGame), game);
		}

		public async Task NextPhase(int gameId)
		{
			Game game = await _dataContext.Games
				.Include(g => g.Players)
				.Include(g => g.CurrentPhase)
				.FirstOrDefaultAsync(g => g.GameId == gameId);

			int currentPlayerId = game.CurrentPlayer.PlayerId;
			bool isLastPhase = game.CurrentPhase.PhaseId == (int)Phases.Morale;
			bool isLastPlayer = currentPlayerId == game.Players.OrderBy(p => p.Initiative).Last().PlayerId; // TODO ...

			// Try to go to next phase
			if (isLastPhase == false)
			{
				game.CurrentPhase = await _dataContext.Phases.FindAsync(game.CurrentPhase.PhaseId + 1);
			}
			else
			{
				game.CurrentPhase = await _dataContext.Phases.FirstAsync();

				// Try to go to next player
				if (isLastPlayer == false)
				{
					game.CurrentPlayer = game.Players
						.OrderBy(p => p.Initiative)
						.SkipWhile(p => p.PlayerId != currentPlayerId)
						.ElementAt(1); // 1 after the current player
				}

				// Go to next turn
				else
				{
					game.CurrentPlayer = game.Players.OrderBy(p => p.Initiative).First();
					game.Turn += 1;
				}

				game.CurrentPlayer.CommandPoints++;

				await Clients.All.SendAsync("UpdatePlayer", game.CurrentPlayer);
			}
			
			await _dataContext.SaveChangesAsync();

			// Echo to all
			await Clients.All.SendAsync(nameof(NextPhase), game);
			//await Clients.Caller.SendAsync(nameof(NextPhase), game);
		}

		public async Task AdjustCommandPoints(int playerId, int amount)
		{
			Player player = await _dataContext.Players.FindAsync(playerId);

			if (player.CommandPoints + amount < 0)
				return;

			player.CommandPoints += amount;

			await _dataContext.SaveChangesAsync();

			await Clients.All.SendAsync("UpdatePlayer", player);
		}

		public async Task AdjustVictoryPoints(int playerId, int amount)
		{
			Player player = await _dataContext.Players.FindAsync(playerId);

			if (player.VictoryPoints + amount < 0)
				return;

			player.VictoryPoints += amount;

			await _dataContext.SaveChangesAsync();

			await Clients.All.SendAsync("UpdatePlayer", player);
		}

		public async Task AddVictoryPoints(VictoryPointsRecord record)
		{
			await _dataContext.VictoryPointsRecords.AddAsync(record);
			await _dataContext.SaveChangesAsync();

			await AdjustVictoryPoints(record.PlayerId, record.Quantity);

			var allRecords = await _dataContext
				.VictoryPointsRecords
				.Where(vpr => vpr.GameId == record.GameId).ToListAsync();
			await Clients.All.SendAsync("AddVictoryPoints", allRecords);
		}
	}
}
