﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using GalaSoft.MvvmLight.Messaging;
using Xamarin.Forms;
using W40kTracker.App.Services;
using W40kTracker.App.ViewModels;
using W40kTracker.App.Views;
using GalaSoft.MvvmLight.Views;
using Microsoft.AspNetCore.SignalR.Client;
using Ninject;
using W40kTracker.App.Messages;
using W40kTracker.App.Models;
using W40kTracker.Data.Models;

namespace W40kTracker.App
{
	public partial class App : Application
	{
		public IKernel Kernel { get; }

		public new static App Current { get; private set; }

		public HubConnection HubConnection { get; set; }

		public App()
		{
			InitializeComponent();

			Current = this;

			HubConnection = new HubConnectionBuilder()
				.WithUrl("http://10.0.1.19:5000/hubs/game")
				.WithAutomaticReconnect()
				.Build();

			Kernel = new StandardKernel(new NinjectSettings { LoadExtensions = false });

			Kernel.Bind<ApiDataStore>().ToSelf();

			// Services
			Kernel.Bind<INavigationService>().To<NavigationService>();

			InitialiseMapper();
		}

		protected override void OnStart()
		{
			RegisterMessages();

			NavigationService.Pages = new Dictionary<string, Page>
			{
				{ nameof(CreateGamePage), new CreateGamePage() },
				{ nameof(JoinGamePage), new JoinGamePage() },
				{ nameof(AddVictoryPointPage), new AddVictoryPointPage() }
			};

			MainPage = new NavigationPage(new ActiveGamesPage());

			Task.Run(async () =>
			{
				await HubConnection.StartAsync();

				HubConnection.Closed += async ex => await MainPage.DisplayAlert("Hub Connection", "Closed", "OK");
				HubConnection.Reconnected += async ex => await MainPage.DisplayAlert("Hub Connection", "Reconnected", "OK");
			});
		}

		protected override void OnSleep()
		{

		}

		protected override void OnResume()
		{
			if (HubConnection.State != HubConnectionState.Connected)
			{
				Task.Run(async () => await HubConnection.StartAsync());
			}

			Task.Run(async () =>
			{
				GameModel currentGame = AppState.Instance?.Game;

				// Game in progress
				if (currentGame?.Turn > 0)
				{
					var mapper = Kernel.Get<IMapper>();
					var store = Kernel.Get<ApiDataStore>();
					Game game = await store.GetGameAsync(currentGame.GameId);
					Messenger.Default.Send(new RefreshMessage<Game>(game));
					
				}
			});
		}

		private void RegisterMessages()
		{
			ViewModelLocator locator = ViewModelLocator.Current;

			// For some reason, this one works...
			Messenger.Default.Register<LoadedMessage>(locator.ActiveGames, nameof(ActiveGamesPage), msg =>
			{
				ActiveGamesViewModel vm = locator.ActiveGames;
				vm.IsBusy = true;
			});

			// But these ones don't work with lambdas
			Messenger.Default.Register<GameSelectedMessage>(locator.JoinGame, locator.JoinGame.OnGameSelected);
			Messenger.Default.Register<LoadedMessage<Game>>(locator.Game, locator.Game.OnGameLoaded);
			Messenger.Default.Register<RefreshMessage<Game>>(locator.InGame, locator.InGame.OnGameRefreshed);
		}



		private void InitialiseMapper()
		{
			var config = new MapperConfiguration(cfg =>
			{
				cfg.CreateMap<Player, PlayerModel>();
				cfg.CreateMap<Game, GameModel>();
			});

			Kernel.Bind<IMapper>().ToConstant(config.CreateMapper());
		}
	}
}
