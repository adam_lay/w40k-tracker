﻿using W40kTracker.Data.Models;

namespace W40kTracker.App.Messages
{
	public class GameSelectedMessage
	{
		public Game Game { get; }

		public GameSelectedMessage(Game game)
		{
			Game = game;
		}
	}
}