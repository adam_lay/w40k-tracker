﻿namespace W40kTracker.App.Messages
{
	public class LoadedMessage
	{
		public LoadedMessage()
		{
		}
	}

	public class LoadedMessage<T>
	{
		public T Parameter { get; }

		public LoadedMessage(T parameter)
		{
			Parameter = parameter;
		}
	}
}
