﻿namespace W40kTracker.App.Messages
{
	public class RefreshMessage<T>
	{
		public T Parameter { get; }

		public RefreshMessage(T parameter)
		{
			Parameter = parameter;
		}
	}
}