﻿
namespace W40kTracker.App.Models
{
	public class PlayerModel : ModelBase
	{
		private int _playerId;
		public int PlayerId
		{
			get => _playerId;
			set => SetProperty(ref _playerId, value);
		}

		private string _name;
		public string Name
		{
			get => _name;
			set => SetProperty(ref _name, value);
		}

		private int? _initiative;
		public int? Initiative
		{
			get => _initiative;
			set => SetProperty(ref _initiative, value);
		}

		private int _commandPoints;
		public int CommandPoints
		{
			get => _commandPoints;
			set => SetProperty(ref _commandPoints, value);
		}

		private int _victoryPoints;
		public int VictoryPoints
		{
			get => _victoryPoints;
			set => SetProperty(ref _victoryPoints, value);
		}
	}
}
