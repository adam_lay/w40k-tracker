﻿
using W40kTracker.Data.Models;

namespace W40kTracker.App.Models
{
	public class GameModel : ModelBase
	{
		private int _gameId;
		public int GameId
		{
			get => _gameId;
			set => SetProperty(ref _gameId, value);
		}

		private string _name;
		public string Name
		{
			get => _name;
			set => SetProperty(ref _name, value);
		}

		private int _turn;
		public int Turn
		{
			get => _turn;
			set => SetProperty(ref _turn, value);
		}

		private Player _currentPlayer;
		public Player CurrentPlayer
		{
			get => _currentPlayer;
			set => SetProperty(ref _currentPlayer, value);
		}

		private Phase _currentPhase;
		public Phase CurrentPhase
		{
			get => _currentPhase;
			set => SetProperty(ref _currentPhase, value);
		}
	}
}
