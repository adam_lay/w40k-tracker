﻿namespace W40kTracker.App.Models
{
	public class VictoryPointSummary
	{
		public int Turn { get; set; }
		public string Player { get; set; }
		public int Primary { get; set; }
		public int Secondary { get; set; }
	}
}
