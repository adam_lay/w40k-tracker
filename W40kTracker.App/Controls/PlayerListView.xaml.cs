﻿using System.Collections.ObjectModel;
using W40kTracker.Data.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace W40kTracker.App.Controls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PlayerListView : CollectionView
	{
		public PlayerListView()
		{
			InitializeComponent();
		}
	}
}