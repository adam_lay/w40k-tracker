﻿using System;
using System.Globalization;
using W40kTracker.App.ViewModels;
using W40kTracker.App.Views;
using Xamarin.Forms;

namespace W40kTracker.App.Converters
{
	public class ViewModelToViewConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is InGameViewModel)
				return new InGameView();
			if (value is PreGameViewModel)
				return new PreGameView();

			return new ContentView
			{
				Content = value == null ? null : new Label { Text = "Unrecognised ViewModel: " + value.GetType().Name }
			};
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
