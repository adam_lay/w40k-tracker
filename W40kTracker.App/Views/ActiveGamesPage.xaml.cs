﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using W40kTracker.App.Messages;
using GalaSoft.MvvmLight.Messaging;
using Xamarin.Forms;

namespace W40kTracker.App.Views
{
	// Learn more about making custom code visible in the Xamarin.Forms previewer
	// by visiting https://aka.ms/xamarinforms-previewer
	[DesignTimeVisible(false)]
	public partial class ActiveGamesPage : ContentPage
	{
		public ActiveGamesPage()
		{
			InitializeComponent();

			this.Appearing += (sender, args) =>
			{
				Messenger.Default.Send(new LoadedMessage(), nameof(ActiveGamesPage));
			};
		}

		//async void OnItemSelected(object sender, EventArgs args)
		//{
		//	var layout = (BindableObject)sender;
		//	var item = (Item)layout.BindingContext;
		//	await Navigation.PushAsync(new ItemDetailPage(new ItemDetailViewModel(item)));
		//}
	}
}