﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace W40kTracker.App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class InGameView : ContentView
	{
		public InGameView()
		{
			InitializeComponent();
		}
	}
}