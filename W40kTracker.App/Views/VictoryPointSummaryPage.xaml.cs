﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace W40kTracker.App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class VictoryPointSummaryPage : ContentPage
	{
		public VictoryPointSummaryPage()
		{
			InitializeComponent();
		}
	}
}