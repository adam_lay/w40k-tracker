﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using W40kTracker.App.Models;
using W40kTracker.App.ViewModels;
using W40kTracker.Data.Models;

namespace W40kTracker.App.Services
{
	public class AppState : BaseViewModel
	{
		private static AppState _instance;
		public static AppState Instance => _instance ?? (_instance = new AppState());

		private AppState() { }

		public PlayerModel Player => Players.FirstOrDefault(p => p.PlayerId == PlayerId);

		private ObservableCollection<PlayerModel> _players = new ObservableCollection<PlayerModel>();
		public ObservableCollection<PlayerModel> Players
		{
			get => _players;
			set => SetProperty(ref _players, value);
		}

		private GameModel _game;
		public GameModel Game
		{
			get => _game;
			set => SetProperty(ref _game, value);
		}

		private int _playerId;
		public int PlayerId
		{
			get => _playerId;
			set
			{
				if (SetProperty(ref _playerId, value))
				{
					RaisePropertyChanged(nameof(Player));
				}
			}
		}

		public int FactionId { get; set; }
		public IEnumerable<Stratagem> Stratagems { get; set; }

		private ObservableCollection<Stratagem> _filteredStratagems;
		public ObservableCollection<Stratagem> FilteredStratagems
		{
			get => _filteredStratagems;
			set => SetProperty(ref _filteredStratagems, value);
		}
	}
}
