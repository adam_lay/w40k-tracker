﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Essentials;
using W40kTracker.Data.Models;
using RestSharp;

namespace W40kTracker.App.Services
{
	public class ApiDataStore
	{
		private readonly HttpClient _client;

		private IEnumerable<Game> _games = new List<Game>();
		private IEnumerable<Player> _players = new List<Player>();
		private RestClient _restClient;

		public string Url { get; set; } = "http://10.0.1.19:5000/api/";

		public ApiDataStore()
		{
			_restClient = new RestClient(Url);

			_client = new HttpClient
			{
				BaseAddress = new Uri(Url)
			};
		}

		private bool IsConnected => Connectivity.NetworkAccess == NetworkAccess.Internet;

		public async Task<IEnumerable<Game>> GetGamesAsync(bool forceRefresh = false)
		{
			if (forceRefresh && IsConnected)
			{
				_games = await _restClient.GetAsync<IEnumerable<Game>>(new RestRequest("games"));
			}

			return _games;
		}

		public async Task<Game> GetGameAsync(int id)
		{
			return await _restClient.GetAsync<Game>(new RestRequest("games/{id}")
			{
				Parameters = { new Parameter("id", id, ParameterType.UrlSegment) }
			});
		}

		public async Task<Game> CreateGameAsync(Game game)
		{
			var req = new RestRequest("games");

			req.AddJsonBody(game);

			return await _restClient.PostAsync<Game>(req);
		}

		public async Task<Player> CreatePlayerAsync(int gameId, string name, int commandPoints, int factionId)
		{
			var req = new RestRequest("players/game/{id}")
			{
				Parameters = { new Parameter("id", gameId, ParameterType.UrlSegment) }
			};

			req.AddJsonBody(new Player
			{
				Name = name,
				CommandPoints = commandPoints,
				FactionId = factionId
			});

			return await _restClient.PostAsync<Player>(req);
		}

		public async Task<IEnumerable<Player>> GetPlayersForGameAsync(int gameId)
		{
			return await _restClient.GetAsync<IEnumerable<Player>>(new RestRequest("players/game/{id}")
			{
				Parameters = { new Parameter("id", gameId, ParameterType.UrlSegment) }
			});
		}

		public async Task<IEnumerable<Faction>> GetFactionsAsync()
		{
			return await _restClient.GetAsync<IEnumerable<Faction>>(new RestRequest("factions"));
		}

		public async Task<IEnumerable<Stratagem>> GetStratagemsAsync(int factionId)
		{
			return await _restClient.GetAsync<IEnumerable<Stratagem>>(new RestRequest("stratagems/{id}")
			{
				Parameters = { new Parameter("id", factionId, ParameterType.UrlSegment) }
			});
		}
	}
}
