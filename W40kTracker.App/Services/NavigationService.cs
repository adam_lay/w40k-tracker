﻿using GalaSoft.MvvmLight.Views;
using System.Collections.Generic;
using Xamarin.Forms;

namespace W40kTracker.App.Services
{
	public class NavigationService : INavigationService
	{
		//public static Page MainPage { get; set; }
		public static Dictionary<string, Page> Pages { get; set; }

		public void GoBack()
		{
			App.Current.MainPage.Navigation.PopModalAsync();
		}

		public void NavigateTo(string pageKey)
		{
			App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(Pages[pageKey]));
		}

		public void NavigateTo(string pageKey, object parameter)
		{
			App.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(Pages[pageKey]));
		}

		public string CurrentPageKey { get; }
	}
}
