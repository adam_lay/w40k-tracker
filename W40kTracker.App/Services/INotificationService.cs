﻿namespace W40kTracker.App.Services
{
	public interface INotificationService
	{
		void ShowToast(string message);
		void ShowSnackbar(string message);
	}
}
