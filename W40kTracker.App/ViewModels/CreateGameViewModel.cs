﻿using System.Windows.Input;
using W40kTracker.App.Messages;
using W40kTracker.App.Services;
using W40kTracker.Data.Models;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Views;

namespace W40kTracker.App.ViewModels
{
	public class CreateGameViewModel : BaseViewModel
	{
		private readonly INavigationService _navigationService;
		private readonly ApiDataStore _dataStore;

		public CreateGameViewModel(INavigationService navigationService, ApiDataStore dataStore)
		{
			_navigationService = navigationService;
			_dataStore = dataStore;

			Title = "Create Game";
			Name = "New Game";
		}

		private string _name;
		public string Name
		{
			get => _name;
			set => SetProperty(ref _name, value);
		}

		public ICommand CreateCommand => new RelayCommand(async () =>
		{
			// Make new game
			Game newGame = await _dataStore.CreateGameAsync(new Game
			{
				Active = true,
				Name = Name
			});

			// Re-load with new game
			Messenger.Default.Send(new LoadedMessage(), "GamePage");

			_navigationService.GoBack();
		});

		public ICommand CancelCommand => new RelayCommand(async () =>
		{
			_navigationService.GoBack();
		});
	}
}