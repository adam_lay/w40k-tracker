﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using AutoMapper;
using W40kTracker.App.Messages;
using W40kTracker.App.Services;
using W40kTracker.Data.Models;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Views;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Internal;
using W40kTracker.App.Models;
using W40kTracker.App.Views;

namespace W40kTracker.App.ViewModels
{
	public class JoinGameViewModel : BaseViewModel
	{
		private readonly INavigationService _navigationService;
		private readonly ApiDataStore _dataStore;
		private readonly IMapper _mapper;

		public JoinGameViewModel(INavigationService navigationService, ApiDataStore dataStore, IMapper mapper)
		{
			_navigationService = navigationService;
			_dataStore = dataStore;
			_mapper = mapper;

			Title = "Join Game";
			Players = new ObservableCollection<Player>();

			// DEBUG
			//Name = "Adam";
			CommandPoints = 3;
		}
		public JoinGameViewModel() { } // For design time

		public Game Game { get; set; }
		public bool ShowExistingPlayers => Players?.Any() == true;
		public bool ShowNewPlayer => Game?.Turn == 0;

		private ObservableCollection<Player> _players;
		public ObservableCollection<Player> Players
		{
			get => _players;
			set => SetProperty(ref _players, value);
		}

		private string _name;
		public string Name
		{
			get => _name;
			set => SetProperty(ref _name, value);
		}

		private int _cp;
		public int CommandPoints
		{
			get => _cp;
			set => SetProperty(ref _cp, value);
		}

		private ObservableCollection<Faction> _factions;
		public ObservableCollection<Faction> Factions
		{
			get => _factions;
			set => SetProperty(ref _factions, value);
		}

		private Faction _selectedFaction;
		public Faction SelectedFaction
		{
			get => _selectedFaction;
			set => SetProperty(ref _selectedFaction, value);
		}

		public ICommand JoinGameAsExistingCommand => new RelayCommand<Player>(async (player) =>
		{
			await JoinGame(player);
		});

		public ICommand JoinGameCommand => new RelayCommand(async () =>
		{
			if (SelectedFaction == null)
			{
				await App.Current.MainPage.DisplayAlert("No Faction", "Select a faction", "OK");
				return;
			}

			Player player = await _dataStore.CreatePlayerAsync(Game.GameId, Name, CommandPoints, SelectedFaction.FactionId);

			await JoinGame(player);
		});

		private async Task JoinGame(Player player)
		{
			AppState.PlayerId = player.PlayerId;
			AppState.FactionId = player.FactionId;

			await App.Current.HubConnection.SendAsync("PlayerJoined", AppState.PlayerId);

			Game game = await _dataStore.GetGameAsync(Game.GameId);

			App.Current.MainPage = new MainPage();

			Messenger.Default.Send(new LoadedMessage<Game>(game));
		}

		public ICommand CancelCommand => new RelayCommand(() =>
		{
			_navigationService.GoBack();
		});

		public async void OnGameSelected(GameSelectedMessage msg)
		{
			JoinGameViewModel vm = this;
			vm.Game = msg.Game;
			vm.Title = "Join " + vm.Game.Name;
			IEnumerable<Player> players = await _dataStore.GetPlayersForGameAsync(vm.Game.GameId);
			vm.Players = new ObservableCollection<Player>(players);

			RaisePropertyChanged(nameof(ShowExistingPlayers));
			RaisePropertyChanged(nameof(ShowNewPlayer));

			var factions = await _dataStore.GetFactionsAsync();
			Factions = new ObservableCollectionListSource<Faction>(factions);

			_navigationService.NavigateTo("JoinGamePage");
		}
	}
}