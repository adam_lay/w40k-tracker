﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using AutoMapper;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using Microsoft.AspNetCore.SignalR.Client;
using W40kTracker.App.Messages;
using W40kTracker.App.Models;
using W40kTracker.App.Services;
using W40kTracker.Data.Models;
using Xamarin.Essentials;

namespace W40kTracker.App.ViewModels
{
	public class VictoryPointSummaryViewModel : BaseViewModel
	{
		public VictoryPointSummaryViewModel()
		{
			App.Current.HubConnection.On<List<VictoryPointsRecord>>("AddVictoryPoints", allRecords =>
			{
				MainThread.BeginInvokeOnMainThread(() =>
				{
					SetTurnSummaries(allRecords);
				});
			});
		}
		//public VictoryPointSummaryViewModel() { }


		private ObservableCollection<VictoryPointSummary> _turnSummaries;
		public ObservableCollection<VictoryPointSummary> TurnSummaries
		{
			get => _turnSummaries;
			set => SetProperty(ref _turnSummaries, value);
		}

		public void SetTurnSummaries(IEnumerable<VictoryPointsRecord> allRecords)
		{
			var summary = allRecords
				.GroupBy(r => new { r.Turn, r.PlayerId })
				.Select(grp => new VictoryPointSummary
				{
					Turn = grp.Key.Turn,
					Player = AppState.Players.FirstOrDefault(p => p.PlayerId == grp.Key.PlayerId)?.Name ?? "No Player",
					Primary = grp.Where(vpr => vpr.Type == VictoryPointType.Primary).Sum(vpr => vpr.Quantity),
					Secondary = grp.Where(vpr => vpr.Type == VictoryPointType.Secondary).Sum(vpr => vpr.Quantity)
				});

			TurnSummaries = new ObservableCollection<VictoryPointSummary>(summary);
		}

		public ICommand AddVictoryPointCommand => new RelayCommand(async () =>
		{
			//_navigationService.NavigateTo("AddVictoryPointPage");

			//await App.Current
			//	.HubConnection
			//	.SendAsync("AdjustVictoryPoints", AppState.PlayerId, 1);
		});
	}
}