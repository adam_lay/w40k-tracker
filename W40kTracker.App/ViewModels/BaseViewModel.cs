﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;

using GalaSoft.MvvmLight;
using W40kTracker.App.Services;

namespace W40kTracker.App.ViewModels
{
	public abstract class BaseViewModel : ViewModelBase
	{
		private bool _isBusy = false;
		public bool IsBusy
		{
			get => _isBusy;
			set => SetProperty(ref _isBusy, value);
		}

		private string _title = string.Empty;
		public string Title
		{
			get => _title;
			set => SetProperty(ref _title, value);
		}

		public AppState AppState => AppState.Instance;

		protected bool SetProperty<T>(ref T backingStore, T value,
				[CallerMemberName] string propertyName = "")
		{
			if (EqualityComparer<T>.Default.Equals(backingStore, value))
				return false;
			
			backingStore = value;
			RaisePropertyChanged(propertyName);
			return true;
		}
	}
}
