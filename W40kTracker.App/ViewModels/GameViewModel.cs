﻿using System.Collections.ObjectModel;
using System.Linq;
using AutoMapper;
using W40kTracker.App.Services;
using Microsoft.AspNetCore.SignalR.Client;
using W40kTracker.App.Messages;
using W40kTracker.App.Models;
using W40kTracker.Data.Models;
using Xamarin.Essentials;

namespace W40kTracker.App.ViewModels
{
	public class GameViewModel : BaseViewModel
	{
		private readonly INotificationService _notificationService;
		private readonly IMapper _mapper;
		private readonly ApiDataStore _dataStore;

		public GameViewModel(INotificationService notificationService,
			IMapper mapper, ApiDataStore dataStore)
		{
			_notificationService = notificationService;
			_mapper = mapper;
			_dataStore = dataStore;

			App.Current.HubConnection.On<Player>("PlayerJoined", result =>
			{
				MainThread.BeginInvokeOnMainThread(() =>
				{
					// If we're not tracking this player, add them
					if (AppState.Players.All(p => p.PlayerId != result.PlayerId))
					{
						AppState.Instance.Players.Add(_mapper.Map<PlayerModel>(result));
					}
					
					_notificationService.ShowSnackbar("Player joined: " + result.Name);
				});
			});

			App.Current.HubConnection.On<int, int>("SetInitiative", (playerId, initiative) =>
			{
				MainThread.BeginInvokeOnMainThread(() =>
				{
					PlayerModel player = AppState.Instance.Players
						.FirstOrDefault(p => p.PlayerId == playerId);

					if (player != null)
						player.Initiative = initiative;
				});
			});

			App.Current.HubConnection.On<Game>("StartGame", (game) =>
			{
				// Ignore if it's not this game... lol unlikely
				if (AppState.Instance.Game?.GameId != game.GameId)
					return;

				MainThread.BeginInvokeOnMainThread(() =>
				{
					AppState.Instance.Game = _mapper.Map<GameModel>(game);
					AppState.Instance.Players = new ObservableCollection<PlayerModel>(game.Players.Select(p => _mapper.Map<PlayerModel>(p)));

					CurrentViewModel = ViewModelLocator.Current.InGame;
				});
			});
		}

		private BaseViewModel _currentViewModel;
		public BaseViewModel CurrentViewModel
		{
			get => _currentViewModel;
			set => SetProperty(ref _currentViewModel, value);
		}

		public async void OnGameLoaded(LoadedMessage<Game> msg)
		{
			Game game = msg.Parameter;
			ViewModelLocator locator = ViewModelLocator.Current;
			GameViewModel vm = this;

			vm.Title = game.Name;

			AppState.Game = _mapper.Map<GameModel>(game);
			AppState.Players = new ObservableCollection<PlayerModel>(game.Players.Select(p => _mapper.Map<PlayerModel>(p)));

			if (game.VictoryPointsRecords != null)
				ViewModelLocator.Current.VictoryPointSummary.SetTurnSummaries(game.VictoryPointsRecords);

			// TODO: Refactor to common location
			AppState.Stratagems = await _dataStore.GetStratagemsAsync(AppState.FactionId);
			AppState.FilteredStratagems = new ObservableCollection<Stratagem>(
				AppState.Stratagems.Where(s => s.PhaseId.HasValue == false || s.PhaseId == game.CurrentPhase.PhaseId));

			vm.CurrentViewModel = game.InProgress
				? (BaseViewModel)locator.InGame
				: (BaseViewModel)locator.PreGame;
		}
	}
}