﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using W40kTracker.App.Messages;
using W40kTracker.App.Services;
using W40kTracker.Data.Models;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Views;
using Microsoft.AspNetCore.SignalR.Client;
using W40kTracker.App.Views;
using Xamarin.Essentials;

namespace W40kTracker.App.ViewModels
{
	public class ActiveGamesViewModel : BaseViewModel
	{
		private readonly INavigationService _navigationService;
		private readonly ApiDataStore _dataStore;
		private readonly INotificationService _notificationService;

		public ActiveGamesViewModel(INavigationService navigationService,
			ApiDataStore dataStore, INotificationService notificationService)
		{
			_navigationService = navigationService;
			_dataStore = dataStore;
			_notificationService = notificationService;

			Title = "Open Games";
			Games = new ObservableCollection<Game>();

			App.Current.HubConnection.On<Game>("NewGame", result =>
			{
				MainThread.BeginInvokeOnMainThread(() =>
				{
					Games.Add(result);

					_notificationService.ShowSnackbar("Game created: " + result.Name);
				});
			});
		}

		public ObservableCollection<Game> Games { get; set; }

		public ICommand CreateCommand => new RelayCommand(() =>
		{
			_navigationService.NavigateTo("CreateGamePage");
		});

		public ICommand GameSelectedCommand => new RelayCommand<Game>((game) =>
		{
			Messenger.Default.Send(new GameSelectedMessage(game));
		});

		private bool _loading;

		public ICommand LoadCommand => new RelayCommand(async () =>
		{
			IsBusy = true;

			if (_loading)
				return;
			
			_loading = true;

			try
			{
				Games.Clear();
				IEnumerable<Game> games = await _dataStore.GetGamesAsync(true);
				foreach (Game game in games)
				{
					Games.Add(game);
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);
			}
			finally
			{
				IsBusy = false;
				_loading = false;
			}
		});


	}
}