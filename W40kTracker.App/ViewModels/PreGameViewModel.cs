﻿using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Microsoft.AspNetCore.SignalR.Client;
using W40kTracker.App.Services;

namespace W40kTracker.App.ViewModels
{
	public class PreGameViewModel : BaseViewModel
	{
		private readonly INotificationService _notificationService;

		public PreGameViewModel(INotificationService notificationService)
		{
			_notificationService = notificationService;
		}

		public ICommand SetInitiativeCommand => new RelayCommand(async () =>
		{
			AppState.Player.Initiative = Initiative;

			// Send update to Update other clients
			await App.Current.HubConnection.SendAsync("SetInitiative", AppState.Player.PlayerId, Initiative);
		});

		public ICommand StartGameCommand => new RelayCommand(async () =>
		{
			// Check if it's ready (player initiative)
			if (AppState.Players.Count < 2)
			{
				_notificationService.ShowToast("More than 1 player required to start game");
				//return;
			}

			if (AppState.Players.Any(p => !p.Initiative.HasValue))
			{
				_notificationService.ShowToast("Not all players are ready");
				return;
			}

			await App.Current.HubConnection.SendAsync("StartGame", AppState.Game.GameId);
		});

		private int _initiative = 5;
		public int Initiative
		{
			get => _initiative;
			set => SetProperty(ref _initiative, value);
		}
	}
}