﻿using System;
using System.Diagnostics;
using Ninject;

namespace W40kTracker.App.ViewModels
{
	public class ViewModelLocator
	{
		public static ViewModelLocator Current { get; private set; }

		public ViewModelLocator()
		{
			Current = this;
		}

		public MenuViewModel Menu => Get<MenuViewModel>();

		public GameViewModel Game => Get<GameViewModel>();
		public InGameViewModel InGame => Get<InGameViewModel>();
		public PreGameViewModel PreGame => Get<PreGameViewModel>();
		public ActiveGamesViewModel ActiveGames => Get<ActiveGamesViewModel>();
		public CreateGameViewModel CreateGame => Get<CreateGameViewModel>();
		public JoinGameViewModel JoinGame => Get<JoinGameViewModel>();
		public AddVictoryPointViewModel AddVictoryPoint => Get<AddVictoryPointViewModel>();
		public VictoryPointSummaryViewModel VictoryPointSummary => Get<VictoryPointSummaryViewModel>();
		public StratagemsViewModel Stratagems => Get<StratagemsViewModel>();

		public static T Get<T>()
		{
			try
			{
				if (!App.Current.Kernel.CanResolve<T>())
					App.Current.Kernel.Bind<T>().ToSelf().InSingletonScope();
				return App.Current.Kernel.Get<T>();
			}
			catch (Exception ex)
			{
				Debug.Write("Failed to bind " + typeof(T).Name);
				// FOR DESIGN TIME ONLY!
				return (T)Activator.CreateInstance(typeof(T));
			}
		}

	}
}
