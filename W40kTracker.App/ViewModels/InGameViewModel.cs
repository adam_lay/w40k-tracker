﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using AutoMapper;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using Microsoft.AspNetCore.SignalR.Client;
using W40kTracker.App.Messages;
using W40kTracker.App.Models;
using W40kTracker.App.Services;
using W40kTracker.Data.Models;
using Xamarin.Essentials;

namespace W40kTracker.App.ViewModels
{
	public class InGameViewModel : BaseViewModel
	{
		private readonly INotificationService _notificationService;
		private readonly IMapper _mapper;
		private readonly INavigationService _navigationService;

		public InGameViewModel(INotificationService notificationService,
			IMapper mapper, INavigationService navigationService)
		{
			_notificationService = notificationService;
			_mapper = mapper;
			_navigationService = navigationService;

			App.Current.HubConnection.On<Game>("NextPhase", game =>
			{
				MainThread.BeginInvokeOnMainThread(async () =>
				{
					if (game.Turn != AppState.Game.Turn)
					{
						await App.Current.MainPage.DisplayAlert("New Turn", "Count VP!", "OK");
					}

					// TODO: Refactor to common location
					// Update stratagems for this phase
					AppState.FilteredStratagems = new ObservableCollection<Stratagem>(
						AppState.Stratagems.Where(s => s.PhaseId.HasValue == false || s.PhaseId == game.CurrentPhase.PhaseId));

					AppState.Game = _mapper.Map<GameModel>(game);
				});
			});

			App.Current.HubConnection.On<Player>("UpdatePlayer", player =>
			{
				MainThread.BeginInvokeOnMainThread(() =>
				{
					PlayerModel localPlayer = AppState.Players
						.First(p => p.PlayerId == player.PlayerId);

					int index = AppState.Players.IndexOf(localPlayer);
					AppState.Players.RemoveAt(index);
					AppState.Players.Insert(index, _mapper.Map<PlayerModel>(player));
				});
			});
		}
		public InGameViewModel() { }


		public ICommand NextPhaseCommand => new RelayCommand(async () =>
		{
			await App.Current
				.HubConnection
				.SendAsync("NextPhase", AppState.Game.GameId);
		});

		public ICommand SpendCommandPointCommand => new RelayCommand(async () =>
		{
			await App.Current
				.HubConnection
				.SendAsync("AdjustCommandPoints", AppState.PlayerId, -1);
		});

		public ICommand GainCommandPointCommand => new RelayCommand(async () =>
		{
			await App.Current
				.HubConnection
				.SendAsync("AdjustCommandPoints", AppState.PlayerId, 1);
		});

		public ICommand AddVictoryPointCommand => new RelayCommand(async () =>
		{
			_navigationService.NavigateTo("AddVictoryPointPage");

			//await App.Current
			//	.HubConnection
			//	.SendAsync("AdjustVictoryPoints", AppState.PlayerId, 1);
		});

		public void OnGameRefreshed(RefreshMessage<Game> msg)
		{
			AppState.Game = _mapper.Map<GameModel>(msg.Parameter);
			AppState.Players = new ObservableCollection<PlayerModel>(msg.Parameter.Players.Select(p => _mapper.Map<PlayerModel>(p)));
		}
	}
}