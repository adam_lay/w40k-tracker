﻿using GalaSoft.MvvmLight.Views;

namespace W40kTracker.App.ViewModels
{
	public class StratagemsViewModel : BaseViewModel
	{
		private readonly INavigationService _navigationService;

		public StratagemsViewModel(INavigationService navigationService)
		{
			_navigationService = navigationService;

			Title = "Stratagems";
		}
	}
}