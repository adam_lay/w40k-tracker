﻿using System.Windows.Input;
using W40kTracker.App.Services;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using Microsoft.AspNetCore.SignalR.Client;
using W40kTracker.Data.Models;

namespace W40kTracker.App.ViewModels
{
	public class AddVictoryPointViewModel : BaseViewModel
	{
		private readonly INavigationService _navigationService;

		public AddVictoryPointViewModel(INavigationService navigationService)
		{
			_navigationService = navigationService;

			Title = "Add Victory Point";
		}

		private int _quantity;
		public int Quantity
		{
			get => _quantity;
			set => SetProperty(ref _quantity, value);
		}

		private bool _isPrimary = true;
		public bool IsPrimary
		{
			get => _isPrimary;
			set => SetProperty(ref _isPrimary, value);
		}

		private bool _isSecondary;
		public bool IsSecondary
		{
			get => _isSecondary;
			set => SetProperty(ref _isSecondary, value);
		}

		public ICommand AddCommand => new RelayCommand(async () =>
		{
			await App.Current.HubConnection.SendAsync("AddVictoryPoints", new VictoryPointsRecord
			{
				GameId = AppState.Game.GameId,
				PlayerId = AppState.PlayerId,
				Turn = AppState.Game.Turn,
				Quantity = Quantity,
				Type = IsPrimary ? VictoryPointType.Primary : VictoryPointType.Secondary
			});

			_navigationService.GoBack();
		});

		public ICommand CancelCommand => new RelayCommand(() =>
		{
			_navigationService.GoBack();
		});
	}
}