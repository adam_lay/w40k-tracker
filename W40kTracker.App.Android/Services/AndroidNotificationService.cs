﻿using Android.App;
using Android.Support.Design.Widget;
using Android.Widget;
using W40kTracker.App.Services;
using Application = Android.App.Application;

namespace W40kTracker.App.Droid.Services
{
	public class AndroidNotificationService : INotificationService
	{
		public void ShowToast(string message)
		{
			Activity activity = MainActivity.Current;
			Toast.MakeText(Application.Context, message, ToastLength.Long).Show();
		}

		public void ShowSnackbar(string message)
		{
			Activity activity = MainActivity.Current;
			Android.Views.View activityRootView = activity.FindViewById(Android.Resource.Id.Content);
			Snackbar.Make(activityRootView, message, Snackbar.LengthLong).Show();
		}
	}
}