﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using W40kTracker.App.Droid.Services;
using W40kTracker.App.Services;

namespace W40kTracker.App.Droid
{
	[Activity(Label = "W40kTracker.App", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		public static MainActivity Current { get; private set; }

		protected override void OnCreate(Bundle savedInstanceState)
		{
			Current = this;

			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;

			base.OnCreate(savedInstanceState);

			Xamarin.Essentials.Platform.Init(this, savedInstanceState);

			global::Xamarin.Forms.Forms.SetFlags("RadioButton_Experimental");
			global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

			var app = new App();

			app.Kernel.Bind<INotificationService>().To<AndroidNotificationService>();

			LoadApplication(app);
		}
		public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
		{
			Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

			base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
		}
	}
}