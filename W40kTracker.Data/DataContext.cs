﻿using System;
using W40kTracker.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace W40kTracker.Data
{
	public class DataContext : DbContext
	{
		public DbSet<Game> Games { get; set; }
		public DbSet<Player> Players { get; set; }
		public DbSet<Phase> Phases { get; set; }
		public DbSet<VictoryPointsRecord> VictoryPointsRecords { get; set; }
		public DbSet<Faction> Factions{ get; set; }
		public DbSet<Stratagem> Stratagems { get; set; }

		public DataContext(DbContextOptions options) : base(options) { }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlServer(@"Server=dev-sql;Database=Warhammer;User ID=sa;Password=0DCFA546ab;");
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			//modelBuilder.Entity<VictoryPointsRecord>(builder =>
			//{
			//	//builder.HasNoKey();
			//	//builder.HasIndex(vpr => new { vpr.GameId, vpr.PlayerId });
			//});

			base.OnModelCreating(modelBuilder);
		}
	}
}
