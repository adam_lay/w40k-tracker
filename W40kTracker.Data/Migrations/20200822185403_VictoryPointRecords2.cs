﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace W40kTracker.Data.Migrations
{
    public partial class VictoryPointRecords2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VictoryPointsRecord_Games_GameId",
                table: "VictoryPointsRecord");

            migrationBuilder.DropForeignKey(
                name: "FK_VictoryPointsRecord_Players_PlayerId",
                table: "VictoryPointsRecord");

            migrationBuilder.DropPrimaryKey(
                name: "PK_VictoryPointsRecord",
                table: "VictoryPointsRecord");

            migrationBuilder.RenameTable(
                name: "VictoryPointsRecord",
                newName: "VictoryPointsRecords");

            migrationBuilder.RenameIndex(
                name: "IX_VictoryPointsRecord_PlayerId",
                table: "VictoryPointsRecords",
                newName: "IX_VictoryPointsRecords_PlayerId");

            migrationBuilder.RenameIndex(
                name: "IX_VictoryPointsRecord_GameId",
                table: "VictoryPointsRecords",
                newName: "IX_VictoryPointsRecords_GameId");

            migrationBuilder.AlterColumn<int>(
                name: "PlayerId",
                table: "VictoryPointsRecords",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "GameId",
                table: "VictoryPointsRecords",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_VictoryPointsRecords",
                table: "VictoryPointsRecords",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_VictoryPointsRecords_Games_GameId",
                table: "VictoryPointsRecords",
                column: "GameId",
                principalTable: "Games",
                principalColumn: "GameId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VictoryPointsRecords_Players_PlayerId",
                table: "VictoryPointsRecords",
                column: "PlayerId",
                principalTable: "Players",
                principalColumn: "PlayerId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VictoryPointsRecords_Games_GameId",
                table: "VictoryPointsRecords");

            migrationBuilder.DropForeignKey(
                name: "FK_VictoryPointsRecords_Players_PlayerId",
                table: "VictoryPointsRecords");

            migrationBuilder.DropPrimaryKey(
                name: "PK_VictoryPointsRecords",
                table: "VictoryPointsRecords");

            migrationBuilder.RenameTable(
                name: "VictoryPointsRecords",
                newName: "VictoryPointsRecord");

            migrationBuilder.RenameIndex(
                name: "IX_VictoryPointsRecords_PlayerId",
                table: "VictoryPointsRecord",
                newName: "IX_VictoryPointsRecord_PlayerId");

            migrationBuilder.RenameIndex(
                name: "IX_VictoryPointsRecords_GameId",
                table: "VictoryPointsRecord",
                newName: "IX_VictoryPointsRecord_GameId");

            migrationBuilder.AlterColumn<int>(
                name: "PlayerId",
                table: "VictoryPointsRecord",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "GameId",
                table: "VictoryPointsRecord",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddPrimaryKey(
                name: "PK_VictoryPointsRecord",
                table: "VictoryPointsRecord",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_VictoryPointsRecord_Games_GameId",
                table: "VictoryPointsRecord",
                column: "GameId",
                principalTable: "Games",
                principalColumn: "GameId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VictoryPointsRecord_Players_PlayerId",
                table: "VictoryPointsRecord",
                column: "PlayerId",
                principalTable: "Players",
                principalColumn: "PlayerId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
