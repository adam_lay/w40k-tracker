﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace W40kTracker.Data.Migrations
{
    public partial class PlayerFaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FactionId",
                table: "Players",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FactionId",
                table: "Players");
        }
    }
}
