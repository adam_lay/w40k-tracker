﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace W40kTracker.Data.Migrations
{
    public partial class Phases : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Initiative",
                table: "Players",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CurrentPhasePhaseId",
                table: "Games",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Phase",
                columns: table => new
                {
                    PhaseId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Phase", x => x.PhaseId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Games_CurrentPhasePhaseId",
                table: "Games",
                column: "CurrentPhasePhaseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Games_Phase_CurrentPhasePhaseId",
                table: "Games",
                column: "CurrentPhasePhaseId",
                principalTable: "Phase",
                principalColumn: "PhaseId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Games_Phase_CurrentPhasePhaseId",
                table: "Games");

            migrationBuilder.DropTable(
                name: "Phase");

            migrationBuilder.DropIndex(
                name: "IX_Games_CurrentPhasePhaseId",
                table: "Games");

            migrationBuilder.DropColumn(
                name: "Initiative",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "CurrentPhasePhaseId",
                table: "Games");
        }
    }
}
