﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace W40kTracker.Data.Migrations
{
    public partial class CurrentPlayer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CurrentPlayerPlayerId",
                table: "Games",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Games_CurrentPlayerPlayerId",
                table: "Games",
                column: "CurrentPlayerPlayerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Games_Players_CurrentPlayerPlayerId",
                table: "Games",
                column: "CurrentPlayerPlayerId",
                principalTable: "Players",
                principalColumn: "PlayerId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Games_Players_CurrentPlayerPlayerId",
                table: "Games");

            migrationBuilder.DropIndex(
                name: "IX_Games_CurrentPlayerPlayerId",
                table: "Games");

            migrationBuilder.DropColumn(
                name: "CurrentPlayerPlayerId",
                table: "Games");
        }
    }
}
