﻿using System.Collections.Generic;

namespace W40kTracker.Data.Models
{
	public class Game
	{
		public int GameId { get; set; }

		public string Name { get; set; }
		public bool Active { get; set; }
		public int Turn { get; set; }
		public bool InProgress => Turn > 0;

		public List<Player> Players { get; set; } = new List<Player>();
		public Player CurrentPlayer { get; set; }
		public Phase CurrentPhase { get; set; }
		public List<VictoryPointsRecord> VictoryPointsRecords { get; set; }
	}
}
