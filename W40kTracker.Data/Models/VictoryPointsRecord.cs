﻿using System.ComponentModel.DataAnnotations;

namespace W40kTracker.Data.Models
{
	public class VictoryPointsRecord
	{
		[Key]
		public int Id { get; set; }

		public VictoryPointType Type { get; set; }
		public int Quantity { get; set; }
		public int Turn { get; set; }

		public int GameId { get; set; }
		public int PlayerId { get; set; }
	}

	public enum VictoryPointType
	{
		Primary,
		Secondary
	}
}
