﻿using System.Collections.Generic;

namespace W40kTracker.Data.Models
{
	public class Player
	{
		public int PlayerId { get; set; }

		public string Name { get; set; }
		public int? Initiative { get; set; }
		public int CommandPoints { get; set; }
		public int VictoryPoints { get; set; }

		public int FactionId { get; set; }

		public List<VictoryPointsRecord> VictoryPointsRecords { get; set; }
	}
}
