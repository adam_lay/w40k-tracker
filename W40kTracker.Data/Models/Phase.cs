﻿namespace W40kTracker.Data.Models
{
	public class Phase
	{
		public int PhaseId { get; set; }
		public string Name { get; set; }
	}

	public enum Phases : int
	{
		Command = 1,
		Movement = 2,
		Psychic = 3,
		Shooting = 4,
		Charge = 5,
		Fight = 6,
		Morale = 7
	}
}
