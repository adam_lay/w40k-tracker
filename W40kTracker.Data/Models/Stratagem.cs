﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace W40kTracker.Data.Models
{
	public class Stratagem
	{
		[Key]
		public int StratagemId { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public int Cost { get; set; }
		public int? PhaseId { get; set; }
		public int FactionId { get; set; }
	}
}
