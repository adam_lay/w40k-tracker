﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace W40kTracker.Data.Models
{
	public class Faction
	{
		[Key]
		public int FactionId { get; set; }
		public string Name { get; set; }

		public List<Stratagem> Stratagems { get; set; }
	}
}
