ECHO OFF
ECHO.
if "%~1"=="" GOTO NO_ARG

dotnet ef migrations add %1 -o Migrations --startup-project ..\W40kTracker.Api\

GOTO DONE

:NO_ARG

ECHO "Usage: add_migrations.bat [migration name]"

GOTO DONE

:DONE
